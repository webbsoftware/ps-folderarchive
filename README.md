# PowerShell - Archive (Unique GUID) Folders as 7Zip Files #

### Usage ###
```
powershell -exec bypass -f ps-folderarchive.ps1 {source-parent-folder} {destination-archive-folder} {earliest-age-in-days}
```
* `{earliest-age-in-days}` value of **90** means that any folders **90 days or older** will be included in the processing.


This was originally created to archive "Package Folders" of a document-builder service that used GUID-named folders for each "Package."
