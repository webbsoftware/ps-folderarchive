﻿#
# ps-filecleanup.ps1
#
param(
	[Parameter(Mandatory=$true)]
	[string]  $sourceFolder, 
	[int]     $ageInDays,
    [string]  $fileMask = "*.*")

#======================================================================================
# Name:				ps-filecleanup.ps1
# Description:		Will remove files older than number of days provided
# Author:           Keven Webb, May, 2018
#======================================================================================


function Get-AgedFiles {
	Param ([string] $sourceFolder, [int] $ageInDays, [string] $fileMask)

	$searchFolder = Join-Path "$sourceFolder" "$fileMask"
	return (Get-ChildItem  -Attributes !Directory -path "$searchFolder" | 
		Where-Object {$_.LastWRiteTime -le (Get-Date).AddDays($ageInDays * -1)} |
			ForEach-Object {$_.FullName } )
}


#*****************************************************************************
# main code
#*****************************************************************************
$counter = 0
Write-Host ("Cleaning files older than " + $ageInDays + " days...")
Get-AgedFiles $sourceFolder $ageInDays $fileMask | Foreach-Object -Process { 
		#Remove-Item -Path $_ -Force
		Write-Host ("   " +  $_ )
		$counter += 1
}
Write-Host "Files removed: $counter"
