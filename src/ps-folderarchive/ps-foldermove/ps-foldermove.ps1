﻿param(
	[Parameter(Mandatory=$true)]
	[string]  $sourceFolder, 
	[string]  $targetFolder, 
	[int]     $ageInDays)

#======================================================================================
# Name:				ps-foldermove.ps1
# Description:		Will move folders to an archive location
#
#                   Assumes source folder contains folders named with GUIDs (unique values)
# Author:           Keven Webb, June, 2018
#======================================================================================

function Get-AgedFolders {
	Param ([string] $sourceFolder, [int] $ageInDays)

	return (get-childitem -directory -path "$sourceFolder" | Where-Object {$_.LastWRiteTime -le (Get-Date).AddDays($ageInDays * -1)} | ForEach-Object {$_.FullName } )
}


function Get-FolderMove {
	Param ([string] $sourceFolder, [string] $targetParentFolder)

	$result = $FALSE

	if ( (Test-Path -Path  $sourceFolder -PathType Container) -and (Test-Path -Path $targetParentFolder -PathType Container) )
	{
		$targetDir = (Split-Path  $sourceFolder -Leaf)
		$targetDir = (Join-Path $targetParentFolder $targetDir)

		Copy-Item -Path $sourceFolder -Recurse -Force -Destination $targetDir
		if ($?)
		{
			Remove-Item -Path $sourceFolder -Force -Recurse
		}
		$result = $?
	}

	return $result
}


#*****************************************************************************
# main code
#*****************************************************************************
$counter = 0
Get-AgedFolders $sourceFolder $ageInDays | Foreach-Object -Process { 

	$showDir = (Split-Path  $_ -Leaf)
	$status  = " Ok"
	if (Get-FolderMove $_ $targetFolder)
	{
		#Remove-Item -Path $_ -Force -Recurse
	   $counter += 1
	}
	else
	{
	   $status  = " FAILED"
	}
	Write-Host ($showDir + " " + $status)

}

Write-Host "Folders moved: $counter"
