﻿param(
	[Parameter(Mandatory=$true)]
	[string]  $sourceFolder, 
	[string]  $archiveFolder, 
	[int]     $ageInDays)

#======================================================================================
# Name:				ps-folderarchive.ps1
# Description:		Will archive folders
#					- create 7Zip archive of each folder 
#					- if 7Zip is successful, remove source folder
#
#                   Assumes source folder contains folders named with GUIDs (unique values)
# Author:           Keven Webb, April, 2018
#======================================================================================


function Get-AgedFolders {
	Param ([string] $sourceFolder, [int] $ageInDays)

	return (get-childitem -directory -path "$sourceFolder" | Where-Object {$_.LastWRiteTime -le (Get-Date).AddDays($ageInDays * -1)} | ForEach-Object {$_.FullName } )
}

function Get-FolderArchive {
	Param ([string] $sourceFolder, [string] $archiveParentFolder)

	$result = $FALSE

	# ensure 7Zip is found
	$7zipPath = ""
	if (test-path "C:\Program Files\7-Zip\7z.exe")       { $7zipPath = "C:\Program Files\7-Zip\7z.exe"       } 
	if (test-path "C:\Program Files (x86)\7-Zip\7z.exe") { $7zipPath = "C:\Program Files (x86)\7-Zip\7z.exe" } 

	if ($7zipPath -eq "") { throw "7z.exe not found" }

	set-alias sz "$7zipPath" 

	if ( (Test-Path -Path  $sourceFolder -PathType Container) -and (Test-Path -Path $archiveParentFolder -PathType Container) )
	{
		$zipfile = ((Split-Path  $sourceFolder -Leaf) + ".7z")
		if (Test-Path -Path $zipfile -PathType leaf) { Remove-Item $zipfile }

		Write-Host ("Archiving to: " + $zipfile)
		$status = sz a -t7z -y -bsp0 -bso0 "$archiveParentFolder\$zipfile" "$sourceFolder\*"
		if ($LASTEXITCODE -eq 0) 
		{ 
			$result = $TRUE 
		}
	}

	return $result
}

#*****************************************************************************
# main code
#*****************************************************************************
$counter = 0
Get-AgedFolders $sourceFolder $ageInDays | Foreach-Object -Process { 
	if (Get-FolderArchive  $_ $archiveFolder)
	{
		Remove-Item -Path $_ -Force -Recurse
		$counter += 1
	}
}
Write-Host "Folders archive: $counter"
